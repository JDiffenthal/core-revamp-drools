package ims.deal_stp;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Wraps implements java.io.Serializable
{

   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label(value = "AllocationAmount")
   private int allocationAmount;
   @org.kie.api.definition.type.Label(value = "AllocationPercent")
   private int allocationPercent;
   @org.kie.api.definition.type.Label(value = "GroupNumber")
   private int groupNumber;
   @org.kie.api.definition.type.Label(value = "InvestmentType")
   private java.lang.String investmentType;
   @org.kie.api.definition.type.Label(value = "Phasein")
   private boolean phasein;
   @org.kie.api.definition.type.Label(value = "PhaseinPercent")
   private int phaseinPercent;
   @org.kie.api.definition.type.Label(value = "TotalInvestmentAmount")
   private int totalInvestmentAmount;
   @org.kie.api.definition.type.Label(value = "WrapName")
   private java.lang.String wrapName;

   public Wraps()
   {
   }

   public int getAllocationAmount()
   {
      return this.allocationAmount;
   }

   public void setAllocationAmount(int allocationAmount)
   {
      this.allocationAmount = allocationAmount;
   }

   public int getAllocationPercent()
   {
      return this.allocationPercent;
   }

   public void setAllocationPercent(int allocationPercent)
   {
      this.allocationPercent = allocationPercent;
   }

   public int getGroupNumber()
   {
      return this.groupNumber;
   }

   public void setGroupNumber(int groupNumber)
   {
      this.groupNumber = groupNumber;
   }

   public java.lang.String getInvestmentType()
   {
      return this.investmentType;
   }

   public void setInvestmentType(java.lang.String investmentType)
   {
      this.investmentType = investmentType;
   }

   public boolean isPhasein()
   {
      return this.phasein;
   }

   public void setPhasein(boolean phasein)
   {
      this.phasein = phasein;
   }

   public int getPhaseinPercent()
   {
      return this.phaseinPercent;
   }

   public void setPhaseinPercent(int phaseinPercent)
   {
      this.phaseinPercent = phaseinPercent;
   }

   public int getTotalInvestmentAmount()
   {
      return this.totalInvestmentAmount;
   }

   public void setTotalInvestmentAmount(int totalInvestmentAmount)
   {
      this.totalInvestmentAmount = totalInvestmentAmount;
   }

   public java.lang.String getWrapName()
   {
      return this.wrapName;
   }

   public void setWrapName(java.lang.String wrapName)
   {
      this.wrapName = wrapName;
   }

   public Wraps(int allocationAmount, int allocationPercent, int groupNumber,
         java.lang.String investmentType, boolean phasein,
         int phaseinPercent, int totalInvestmentAmount,
         java.lang.String wrapName)
   {
      this.allocationAmount = allocationAmount;
      this.allocationPercent = allocationPercent;
      this.groupNumber = groupNumber;
      this.investmentType = investmentType;
      this.phasein = phasein;
      this.phaseinPercent = phaseinPercent;
      this.totalInvestmentAmount = totalInvestmentAmount;
      this.wrapName = wrapName;
   }

}