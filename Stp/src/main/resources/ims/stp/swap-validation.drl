package ims.stp;

import ims.stp.StpSwitchValidationObject;
import ims.stp.Instruction;
import ims.stp.DestinationInstrument;
import ims.stp.InstrumentInfo;
import ims.stp.TransactionInProgress;
import ims.stp.Switch;
import ims.stp.Deal;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

function boolean evaluateSwitchDate(String switchDate){
	    boolean failRule = false;
	     
	    if(switchDate== null) return true;
	  
	   try{
	         Date today = new Date();        
	         SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy"); 
	         Date swDate= formatter.parse(switchDate);        

	        String strDate = formatter .format(today);
	        Date tDate = formatter.parse(strDate);
	        
	        if(tDate.compareTo(swDate)>0){
	        	return true;
	        }

	      }catch(ParseException ex){
		failRule  = true;
	    	ex.printStackTrace();
	     }
	     return failRule;
}

rule "If Broker corp (FSP) on XML data is Catergory 2 and submitted by the Broker OR if submitted by Investor OR CAT 1 and online auth (Rule 1)"
    when
        m : StpSwitchValidationObject(brokerStat == "N" && inor != "STPSC")
    then
       m.getMessages().add( "The financial advisor does not have the applicable FAIS license to submit this instruction. " );
       m.setFailRules(true);
       m.setFailSigPage(true);       
end

rule "If product is GIB OR GAPU (Rule 2)"
    when
         m : StpSwitchValidationObject()
          i :Instruction() from m.getInstructions().get(0)
	  d : Deal(productCode == "GIB" || productCode == "GAPU" ) from i.getDeals()
    then  
    	 m.getMessages().add( "Global Select switch instructions with deal[" + d.getDealNumber() + "] will not be processed via STP."  ); 
    	 m.setFailRules(true); 
         m.setFailOnOthers(true); 
end

rule "Reject all rand and unit value switches over multiple deals to manual processing"
    when
       m : StpSwitchValidationObject()
       i :Instruction() from m.getInstructions().get(0)
       s :Switch() from i.getSwitches()
       si: SourceInstrument(sourceUserSelection == "R" || sourceUserSelection == "U") from s.getSourceInstruments()
       eval(i.getDeals().size() > 1)
    then  
    	 m.getMessages().add( "Rand and Unit value switches over multiple deals." );    
        m.setFailRules(true);
	m.setFailOnOthers(true);  
end

rule "Switch signature page required(Rule 3) "
    when
       m : StpSwitchValidationObject(signaturePageRequired== true && inor != "STPSC")       
    then  
    	 m.getMessages().add( "Switch signature page is required and cannot be processed via STP " );
         m.setFailSigPage(true);  
    	 m.setFailRules(true); 
end

rule "If there an embargo on the deal(Rule 4) "
    when
      m : StpSwitchValidationObject()
      i :Instruction() from m.getInstructions().get(0)
      d : Deal(embargoRestrictions== true ) from i.getDeals()
    then  
    	 m.getMessages().add( "Switch with deal[" + d.getDealNumber() + "] cannot be processed because there is an active embargo." ); 
    	 m.setFailRules(true); 
         m.setFailOnOthers(true); 
end

rule "No expired instruments are allowed  (Rule 5)) "
    when
       m : StpSwitchValidationObject()
       i :  InstrumentInfo(expiredFund== true ) from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "An expired instrument[" + i.getInstrumentNumber() + "] has been selected and cannot be processed via STP." ); 
    	m.setFailRules(true); 
        m.setFailOnOthers(true); 
end

rule "A switch  out instruction lists an instrument over R5 million as a source instrument is not allowed(Rule 6) "
    when
       m : StpSwitchValidationObject()
       i: InstrumentInfo(totalSwitchAmount >= 5000000.0 && direction =='S') from m.getInstrumentsInfo()     
    then  
    	 m.getMessages().add( "Switch amount on the source instrument[" + i.getInstrumentNumber() + "] exceeds R5million and cannot be processed via STP." ); 
    	m.setFailRules(true); 
        m.setFailOnOthers(true); 
end

//rule "Income freeze period (Rule 7)) "
//    when
 //      m : StpSwitchValidationObject()
//       i : InstrumentInfo(incomeFreeze== true  && incomeAffected == true && direction =='S') from m.getInstrumentsInfo()
//    then  
//    	 m.getMessages().add( "The selected instrument[" + i.getInstrumentNumber() + "] cannot be processed via STP due to income freeze." ); 
//    	m.setFailRules(true); 
//        m.setFailOnOthers(true); 
//end

rule "Any upriced transactions other than a fee must be checked (Rule 8) ) "
    when
       m : StpSwitchValidationObject()
       t : TransactionInProgress(status != "Completed" && !(status == "In- Progress" && flowType == "G")) from m.getTransactionsInProgress()     
    then  
    	 m.getMessages().add( "The selected instrument[" + t.getInstrumentNo() + "] cannot be processed via STP due to unpriced transactions in progress" ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "Change to broker fees are  not allowed (Rule 9) "
    when
       m : StpSwitchValidationObject(changeBrokerFees == true)
    then  
    	 m.getMessages().add( "Broker Fees have been selected and cannot be processed via STP." ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "Model Portfolio is not allowed in source instruction (Rule 10)"
    when
       m : StpSwitchValidationObject()
       i :Instruction() from m.getInstructions().get(0)
       d : Deal(modelPortfolio== true ) from i.getDeals()
    then  
    	 m.getMessages().add( "A Model Portfolio has been selected as a source instrument and cannot be processed via STP." );    
        m.setFailRules(true);
	m.setFailOnOthers(true);  
end

rule "Model Portfolio is not allowed in destination instrument (Rule 10b )"
    when
       m : StpSwitchValidationObject()
       i :Instruction() from m.getInstructions()
       s :Switch() from i.getSwitches()
       ds : DestinationInstrument(fundGroupCode == "MP") from s.getDestinationInstruments()     
    then  
    	 m.getMessages().add( "A Model Portfolio has been selected as a destination instrument[" + ds.getDestinationInstrumentNumber() + "] and cannot be processed via STP. " );    
         m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "No share portfolio allowed(Rule 11) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(instrumentType== "PSP") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A Share Portfolio has been selected on instrument[" + i.getInstrumentNumber() + "] and cannot be processed via STP. "); 
    	 m.setFailRules(true);
	m.setFailOnOthers(true);  
end

rule "Broker funds are only available to investors with the financial planner linked to their investment. (Rule 12) "
    when
       m : StpSwitchValidationObject()
       i :  InstrumentInfo(brokerFund== true && brokerFundAllowed ==false && direction =="D") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A broker fund[" + i.getInstrumentNumber() + "] has been selected, which is not linked to the investor's Financial Advisor and cannot be processed via STP." ); 
    	 m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "No closed(capped) are allowed on destination instrument (Rule 13) "
    when
       m : StpSwitchValidationObject()
        i : InstrumentInfo(closed== true && direction == "D") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A closed(capped) destination instrument[" + i.getInstrumentNumber() + "] has been selected and cannot be processed via STP." ); 
    	 m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "No rationalized  instruments are allowed on destination(Rule 14) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(rationalisedFund== true && direction == "D" ) from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A rationalised destination instrument[" + i.getInstrumentNumber() + "] has been selected and cannot be processed via STP." ); 
    	 m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "Direct investors are not allowed to invest into Fund Select instruments (Rule 15) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(directInvestor== true &&  direction == "D" && (instrumentType == "FSUT" || instrumentType == "FSCAL")) from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A fund select destination instrument[" + i.getInstrumentNumber() + "] has been selected for a direct client and cannot be processed via STP." ); 
    	 m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "Restricted funds are only available to investors with the financial planner linked to their investment. (Rule 16) "
    when
       m : StpSwitchValidationObject()
        i : InstrumentInfo(restrictedFund== true && restrictedFundAllowed ==false && direction == "D") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A restricted destination instrument[" + i.getInstrumentNumber() + "] has been selected, which is not linked to the investor's Financial Advisor and cannot be processed via STP." ); 
    	 m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "If a switch out instruction lists an IAL or IEB life instrument  as a source instrument, then request liquidity before processing.  (Life fund) (Rule 17) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(lifeFund== true && direction == "S") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A IEB or IAL source instrument[" + i.getInstrumentNumber() + "] has been selected and cannot be processed via STP." ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "If the switch includes a notice instrument, notice must be sent to the management company(Rule 18) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(noticeInstrument== true) from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "A notice source instrument[" + i.getInstrumentNumber() + "] has been selected and cannot be processed via STP. " ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "Destination isntrument must be allowed on the Product(Rule 19) "
    when
       m : StpSwitchValidationObject()
       i : InstrumentInfo(productAllowed == false && direction == "D") from m.getInstrumentsInfo()
    then  
    	 m.getMessages().add( "The destination instrument[" + i.getInstrumentNumber() + "] selected is not allowed on this product and cannot be processed via STP. " ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

//rule "No freeze period for debit order on switches from source instruments. (Rule 20) "
//    when
//       m : StpSwitchValidationObject()
//       i :  InstrumentInfo(debitOrderFreeze== true && debitOrderAffected == true && direction == "S") from m.getInstrumentsInfo()
//    then  
//    	 m.getMessages().add( "The selected source instrument[" + i.getInstrumentNumber() + "] cannot be processed via STP due to debit order freeze. " ); 
//    	m.setFailRules(true); 
//	m.setFailOnOthers(true); 
//end

rule "If cancel phase in is selected, send for manual processing (Rule 21) "
    when
       m : StpSwitchValidationObject(cancelPhaseIn== "YES")      
    then  
    	 m.getMessages().add( "Cancel phase-in has been selected, switch cannot be processed via STP  " ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end

rule "If switch date is back date send for manual procesing (Rule 21) "
    when
       m : StpSwitchValidationObject()
       eval(evaluateSwitchDate(m.getSwitchDate()))      
    then  
    	 m.getMessages().add( "Switch date is back dated, switch cannot be processed via STP  " ); 
    	m.setFailRules(true); 
	m.setFailOnOthers(true); 
end