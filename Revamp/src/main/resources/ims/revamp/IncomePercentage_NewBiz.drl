package ims.revamp;

import java.math.BigDecimal;

function BigDecimal GetAnnualIncomePercentage(BigDecimal grossRandValue, String frequency, BigDecimal totalAmount){
        
	double iFrequency = 0.0d;   	
	    	
		if(frequency.compareTo("Q") == 0)
			iFrequency = 4.0;
		else if(frequency.compareTo("M") == 0)
			iFrequency = 12.0;
		else if(frequency.compareTo("H") == 0)
			iFrequency = 2.0;
		else if(frequency.compareTo("HY") == 0)
			iFrequency = 2.0;
		else if(frequency.compareTo("Y") == 0)
			iFrequency = 1.0;
		else if(frequency.compareTo("A") == 0)
			iFrequency = 1.0;

		double dealPercentage = grossRandValue.doubleValue() * iFrequency / totalAmount.doubleValue() * 100.0d;
		
	    return new BigDecimal(dealPercentage).setScale(10, BigDecimal.ROUND_HALF_DOWN);
}
    
function boolean isIncomePercentageInRange(BigDecimal incomePercentage, IncomePercentage_NewBiz m) {

	String[] values = m.getIncomePercentageOutput().split(",", -1);
    	
	BigDecimal LowerThreshold = new BigDecimal(Double.parseDouble(values[0].isEmpty() ? "0" : values[0]));
	BigDecimal UpperThreshold = new BigDecimal(Double.parseDouble(values[1].isEmpty() ? "0" : values[1]));
	
	return ((incomePercentage.compareTo(LowerThreshold) >= 0) 
		&& (incomePercentage.compareTo(UpperThreshold) <= 0));
}
        
function boolean checkIncomePercentage(IncomePercentage_NewBiz m) { 
	String[] values = m.getIncomePercentageOutput().split(",", -1);
    	
	BigDecimal LowerThreshold = new BigDecimal(Double.parseDouble(values[0].isEmpty() ? "0" : values[0]));
	BigDecimal UpperThreshold = new BigDecimal(Double.parseDouble(values[1].isEmpty() ? "0" : values[1]));
	
	String errorMessageForValue = "The calculated income percentage is not within the allowed range, please enter an income percentage between " + LowerThreshold + "% and " + UpperThreshold + "%";
	String errorMessageForPercent = "The specified income percentage is not within the allowed range, please enter an income percentage between " + LowerThreshold + "% and " + UpperThreshold + "%";
	
	if (m.getIsIncomePercentage()) {
		if (!isIncomePercentageInRange(m.getIncomePercentage(), m)) {
			m.setMessage(errorMessageForPercent);
			return true;
		}
		
		return false;
	}
	
	BigDecimal incomeValue = m.getIncomeValue(); 
	BigDecimal investmentAmount = m.getInvestmentAmount(); 
    	
	if (incomeValue.compareTo(BigDecimal.ZERO) > 0 
	&& investmentAmount.compareTo(BigDecimal.ZERO) > 0){        
		BigDecimal calculatedIncomePercentage = GetAnnualIncomePercentage(incomeValue, m.getFrequency().toUpperCase(), investmentAmount);
	    
	    if (!isIncomePercentageInRange(calculatedIncomePercentage, m)) {
	    	m.setMessage(errorMessageForValue);
	    	return true;
	    }		    
	}
	
   	return false;
}   

rule "Income Percentage"
    when 
    	m : IncomePercentage_NewBiz()
    	eval(m.getIsIncomeSelectionApplicable())
	eval(checkIncomePercentage(m))
    then    
    	m.setCode(13);
		m.setSeverity("Critical");
		m.setElement("GrossRandValueIncome");
		m.setFail(true);
end