package ims.revamp;

public class OneTwentyPercent implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   private java.math.BigDecimal newLumpsumInvestmentAmount;

   private java.math.BigDecimal newDebitOrderAmount;

   private java.lang.String frequency;

   private java.math.BigDecimal escalationPercent;

   private java.lang.String escalationDate;

   private java.lang.Boolean escalationApplicable;

   private java.lang.String existingInvestment;

   private java.lang.String debitStartMonth;

   private java.lang.String debitStartYear;

   private java.lang.Boolean isDebitOrderApplicable;

   public OneTwentyPercent()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.math.BigDecimal getNewLumpsumInvestmentAmount()
   {
      return this.newLumpsumInvestmentAmount;
   }

   public void setNewLumpsumInvestmentAmount(
         java.math.BigDecimal newLumpsumInvestmentAmount)
   {
      this.newLumpsumInvestmentAmount = newLumpsumInvestmentAmount;
   }

   public java.math.BigDecimal getNewDebitOrderAmount()
   {
      return this.newDebitOrderAmount;
   }

   public void setNewDebitOrderAmount(java.math.BigDecimal newDebitOrderAmount)
   {
      this.newDebitOrderAmount = newDebitOrderAmount;
   }

   public java.lang.String getFrequency()
   {
      return this.frequency;
   }

   public void setFrequency(java.lang.String frequency)
   {
      this.frequency = frequency;
   }

   public java.math.BigDecimal getEscalationPercent()
   {
      return this.escalationPercent;
   }

   public void setEscalationPercent(java.math.BigDecimal escalationPercent)
   {
      this.escalationPercent = escalationPercent;
   }

   public java.lang.String getEscalationDate()
   {
      return this.escalationDate;
   }

   public void setEscalationDate(java.lang.String escalationDate)
   {
      this.escalationDate = escalationDate;
   }

   public java.lang.Boolean getEscalationApplicable()
   {
      return this.escalationApplicable;
   }

   public void setEscalationApplicable(java.lang.Boolean escalationApplicable)
   {
      this.escalationApplicable = escalationApplicable;
   }

   public java.lang.String getExistingInvestment()
   {
      return this.existingInvestment;
   }

   public void setExistingInvestment(java.lang.String existingInvestment)
   {
      this.existingInvestment = existingInvestment;
   }

   public java.lang.String getDebitStartMonth()
   {
      return this.debitStartMonth;
   }

   public void setDebitStartMonth(java.lang.String debitStartMonth)
   {
      this.debitStartMonth = debitStartMonth;
   }

   public java.lang.String getDebitStartYear()
   {
      return this.debitStartYear;
   }

   public void setDebitStartYear(java.lang.String debitStartYear)
   {
      this.debitStartYear = debitStartYear;
   }

   public java.lang.Boolean getIsDebitOrderApplicable()
   {
      return this.isDebitOrderApplicable;
   }

   public void setIsDebitOrderApplicable(java.lang.Boolean isDebitOrderApplicable)
   {
      this.isDebitOrderApplicable = isDebitOrderApplicable;
   }

   public OneTwentyPercent(java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String severity,
         java.math.BigDecimal newLumpsumInvestmentAmount,
         java.math.BigDecimal newDebitOrderAmount, java.lang.String frequency,
         java.math.BigDecimal escalationPercent,
         java.lang.String escalationDate,
         java.lang.Boolean escalationApplicable,
         java.lang.String existingInvestment, java.lang.String debitStartMonth,
         java.lang.String debitStartYear,
         java.lang.Boolean isDebitOrderApplicable)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.newLumpsumInvestmentAmount = newLumpsumInvestmentAmount;
      this.newDebitOrderAmount = newDebitOrderAmount;
      this.frequency = frequency;
      this.escalationPercent = escalationPercent;
      this.escalationDate = escalationDate;
      this.escalationApplicable = escalationApplicable;
      this.existingInvestment = existingInvestment;
      this.debitStartMonth = debitStartMonth;
      this.debitStartYear = debitStartYear;
      this.isDebitOrderApplicable = isDebitOrderApplicable;
   }
}