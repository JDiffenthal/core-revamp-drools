package ims.revamp;

public class AccessSurrenderPP implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Description("The products shortname")
   @org.kie.api.definition.type.Label("Product Short Name")
   private java.lang.String shortName;

   @org.kie.api.definition.type.Label("Total Existing Loans")
   private java.lang.Integer totalLoans;

   @org.kie.api.definition.type.Label("Total Existing Surrenders")
   private java.lang.Integer totalSurrenders;

   @org.kie.api.definition.type.Label("Inception Date")
   private String inceptionDate;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   private java.lang.String transactionType;

   public AccessSurrenderPP()
   {
   }

   public java.lang.String getShortName()
   {
      return this.shortName;
   }

   public void setShortName(java.lang.String shortName)
   {
      this.shortName = shortName;
   }

   public java.lang.Integer getTotalLoans()
   {
      return this.totalLoans;
   }

   public void setTotalLoans(java.lang.Integer totalLoans)
   {
      this.totalLoans = totalLoans;
   }

   public java.lang.Integer getTotalSurrenders()
   {
      return this.totalSurrenders;
   }

   public void setTotalSurrenders(java.lang.Integer totalSurrenders)
   {
      this.totalSurrenders = totalSurrenders;
   }

   public java.lang.String getInceptionDate()
   {
      return this.inceptionDate;
   }

   public void setInceptionDate(java.lang.String inceptionDate)
   {
      this.inceptionDate = inceptionDate;
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.lang.String getTransactionType()
   {
      return this.transactionType;
   }

   public void setTransactionType(java.lang.String transactionType)
   {
      this.transactionType = transactionType;
   }

   public AccessSurrenderPP(java.lang.String shortName,
         java.lang.Integer totalLoans, java.lang.Integer totalSurrenders,
         java.lang.String inceptionDate, java.lang.String message,
         java.lang.Boolean fail, java.lang.String element,
         java.lang.Integer code, java.lang.String severity,
         java.lang.String transactionType)
   {
      this.shortName = shortName;
      this.totalLoans = totalLoans;
      this.totalSurrenders = totalSurrenders;
      this.inceptionDate = inceptionDate;
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.transactionType = transactionType;
   }
}