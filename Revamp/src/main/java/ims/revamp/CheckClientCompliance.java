package ims.revamp;

public class CheckClientCompliance implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   private java.lang.Boolean isExistingClient;

   @org.kie.api.definition.type.Label("Total compliance records from the data query")
   private java.lang.Integer complianceCount;

   public CheckClientCompliance()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.lang.Boolean getIsExistingClient()
   {
      return this.isExistingClient;
   }

   public void setIsExistingClient(java.lang.Boolean isExistingClient)
   {
      this.isExistingClient = isExistingClient;
   }

   public java.lang.Integer getComplianceCount()
   {
      return this.complianceCount;
   }

   public void setComplianceCount(java.lang.Integer complianceCount)
   {
      this.complianceCount = complianceCount;
   }

   public CheckClientCompliance(java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String severity, java.lang.Boolean isExistingClient,
         java.lang.Integer complianceCount)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.isExistingClient = isExistingClient;
      this.complianceCount = complianceCount;
   }
}