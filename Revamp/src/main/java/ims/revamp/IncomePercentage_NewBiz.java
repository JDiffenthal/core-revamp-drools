package ims.revamp;

public class IncomePercentage_NewBiz implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("IncomePercentageOutput")
   @org.kie.api.definition.type.Description("Comma delimited values for nbLowPerc,nbUpPerc,currentLowPerc,currentUpPerc,dealValue")
   private String incomePercentageOutput;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   @org.kie.api.definition.type.Label("IncomeValue")
   private java.math.BigDecimal incomeValue;

   @org.kie.api.definition.type.Label("InvestmentAmount")
   private java.math.BigDecimal investmentAmount;

   @org.kie.api.definition.type.Label("Frequency")
   private java.lang.String frequency;

   @org.kie.api.definition.type.Label("IsIncomeSelectionApplicable")
   private java.lang.Boolean isIncomeSelectionApplicable;

   @org.kie.api.definition.type.Label("IsIncomePercentage")
   private java.lang.Boolean isIncomePercentage;

   @org.kie.api.definition.type.Label("IncomePercentage")
   private java.math.BigDecimal incomePercentage;

   public IncomePercentage_NewBiz()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.lang.String getIncomePercentageOutput()
   {
      return this.incomePercentageOutput;
   }

   public void setIncomePercentageOutput(java.lang.String incomePercentageOutput)
   {
      this.incomePercentageOutput = incomePercentageOutput;
   }

   public java.math.BigDecimal getInvestmentAmount()
   {
      return this.investmentAmount;
   }

   public void setInvestmentAmount(java.math.BigDecimal investmentAmount)
   {
      this.investmentAmount = investmentAmount;
   }

   public java.lang.String getFrequency()
   {
      return this.frequency;
   }

   public void setFrequency(java.lang.String frequency)
   {
      this.frequency = frequency;
   }

   public java.math.BigDecimal getIncomeValue()
   {
      return this.incomeValue;
   }

   public void setIncomeValue(java.math.BigDecimal incomeValue)
   {
      this.incomeValue = incomeValue;
   }

   public java.lang.Boolean getIsIncomeSelectionApplicable()
   {
      return this.isIncomeSelectionApplicable;
   }

   public void setIsIncomeSelectionApplicable(
         java.lang.Boolean isIncomeSelectionApplicable)
   {
      this.isIncomeSelectionApplicable = isIncomeSelectionApplicable;
   }

   public java.lang.Boolean getIsIncomePercentage()
   {
      return this.isIncomePercentage;
   }

   public void setIsIncomePercentage(java.lang.Boolean isIncomePercentage)
   {
      this.isIncomePercentage = isIncomePercentage;
   }

   public java.math.BigDecimal getIncomePercentage()
   {
      return this.incomePercentage;
   }

   public void setIncomePercentage(java.math.BigDecimal incomePercentage)
   {
      this.incomePercentage = incomePercentage;
   }

   public IncomePercentage_NewBiz(java.lang.String incomePercentageOutput,
         java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String severity, java.math.BigDecimal incomeValue,
         java.math.BigDecimal investmentAmount, java.lang.String frequency,
         java.lang.Boolean isIncomeSelectionApplicable,
         java.lang.Boolean isIncomePercentage,
         java.math.BigDecimal incomePercentage)
   {
      this.incomePercentageOutput = incomePercentageOutput;
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.incomeValue = incomeValue;
      this.investmentAmount = investmentAmount;
      this.frequency = frequency;
      this.isIncomeSelectionApplicable = isIncomeSelectionApplicable;
      this.isIncomePercentage = isIncomePercentage;
      this.incomePercentage = incomePercentage;
   }
}