package ims.revamp;

public class ValidateSharePortfolioShare implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   private java.math.BigDecimal amount;

   private java.math.BigDecimal PSPPerc;

   private java.math.BigDecimal incomeAmt;

   private java.math.BigDecimal incomePerc;

   private java.lang.String PSPType;

   private java.lang.Boolean applyIncome;

   private String frequency;

   private java.lang.String incomeType;

   private java.lang.Integer productGroupId;

   public ValidateSharePortfolioShare()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.lang.String getIncomeType()
   {
      return this.incomeType;
   }

   public void setIncomeType(java.lang.String incomeType)
   {
      this.incomeType = incomeType;
   }

   public java.lang.Integer getProductGroupId()
   {
      return this.productGroupId;
   }

   public void setProductGroupId(java.lang.Integer productGroupId)
   {
      this.productGroupId = productGroupId;
   }

   public java.math.BigDecimal getIncomeAmt()
   {
      return this.incomeAmt;
   }

   public void setIncomeAmt(java.math.BigDecimal incomeAmt)
   {
      this.incomeAmt = incomeAmt;
   }

   public java.math.BigDecimal getIncomePerc()
   {
      return this.incomePerc;
   }

   public void setIncomePerc(java.math.BigDecimal incomePerc)
   {
      this.incomePerc = incomePerc;
   }

   public java.math.BigDecimal getAmount()
   {
      return this.amount;
   }

   public void setAmount(java.math.BigDecimal amount)
   {
      this.amount = amount;
   }

   public java.math.BigDecimal getPSPPerc()
   {
      return this.PSPPerc;
   }

   public void setPSPPerc(java.math.BigDecimal PSPPerc)
   {
      this.PSPPerc = PSPPerc;
   }

   public java.lang.String getPSPType()
   {
      return this.PSPType;
   }

   public void setPSPType(java.lang.String PSPType)
   {
      this.PSPType = PSPType;
   }

   public java.lang.Boolean getApplyIncome()
   {
      return this.applyIncome;
   }

   public void setApplyIncome(java.lang.Boolean applyIncome)
   {
      this.applyIncome = applyIncome;
   }

   public java.lang.String getFrequency()
   {
      return this.frequency;
   }

   public void setFrequency(java.lang.String frequency)
   {
      this.frequency = frequency;
   }

   public ValidateSharePortfolioShare(java.lang.String message,
         java.lang.Boolean fail, java.lang.String element,
         java.lang.Integer code, java.lang.String severity,
         java.math.BigDecimal amount, java.math.BigDecimal PSPPerc,
         java.math.BigDecimal incomeAmt, java.math.BigDecimal incomePerc,
         java.lang.String PSPType, java.lang.Boolean applyIncome,
         java.lang.String frequency, java.lang.String incomeType,
         java.lang.Integer productGroupId)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.amount = amount;
      this.PSPPerc = PSPPerc;
      this.incomeAmt = incomeAmt;
      this.incomePerc = incomePerc;
      this.PSPType = PSPType;
      this.applyIncome = applyIncome;
      this.frequency = frequency;
      this.incomeType = incomeType;
      this.productGroupId = productGroupId;
   }
}