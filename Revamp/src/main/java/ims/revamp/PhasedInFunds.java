package ims.revamp;

public class PhasedInFunds implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   @org.kie.api.definition.type.Label("Comma sepparated list of funds to be checked")
   private java.lang.String fundList;

   @org.kie.api.definition.type.Label("Comma sepparated list of Phased Funds to check against")
   private java.lang.String phasedFunds;

   @org.kie.api.definition.type.Label(value = "Check if Phase In is applicable")
   private java.lang.Boolean isPhaseIn;

   public PhasedInFunds()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.lang.String getFundList()
   {
      return this.fundList;
   }

   public void setFundList(java.lang.String fundList)
   {
      this.fundList = fundList;
   }

   public java.lang.String getPhasedFunds()
   {
      return this.phasedFunds;
   }

   public void setPhasedFunds(java.lang.String phasedFunds)
   {
      this.phasedFunds = phasedFunds;
   }

   public java.lang.Boolean getIsPhaseIn()
   {
      return this.isPhaseIn;
   }

   public void setIsPhaseIn(java.lang.Boolean isPhaseIn)
   {
      this.isPhaseIn = isPhaseIn;
   }

   public PhasedInFunds(java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String severity, java.lang.String fundList,
         java.lang.String phasedFunds, java.lang.Boolean isPhaseIn)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.fundList = fundList;
      this.phasedFunds = phasedFunds;
      this.isPhaseIn = isPhaseIn;
   }
}