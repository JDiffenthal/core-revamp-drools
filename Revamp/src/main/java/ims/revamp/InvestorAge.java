package ims.revamp;

public class InvestorAge implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code = 0;

   @org.kie.api.definition.type.Label("Investors Date of Birth")
   private java.lang.String dateOfBirth;

   @org.kie.api.definition.type.Label(value = "The severity of the rule failure")
   private java.lang.String severity;

   public InvestorAge()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getDateOfBirth()
   {
      return this.dateOfBirth;
   }

   public void setDateOfBirth(java.lang.String dateOfBirth)
   {
      this.dateOfBirth = dateOfBirth;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public InvestorAge(java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String dateOfBirth, java.lang.String severity)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.dateOfBirth = dateOfBirth;
      this.severity = severity;
   }
}