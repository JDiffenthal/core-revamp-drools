package ims.revamp;

public class DebitOrderMinimum implements java.io.Serializable
{
   static final long serialVersionUID = 1L;

   @org.kie.api.definition.type.Label("Response message")
   private String message = "";

   @org.kie.api.definition.type.Label("Indicates if the rule failed")
   private java.lang.Boolean fail = false;;

   @org.kie.api.definition.type.Label("The Element that caused the rule to fail")
   private java.lang.String element;

   @org.kie.api.definition.type.Label("The failure code of the rule")
   private java.lang.Integer code;

   @org.kie.api.definition.type.Label("The severity of the rule failure")
   private java.lang.String severity;

   @org.kie.api.definition.type.Label("The Investment Amount")
   private java.math.BigDecimal investmentAmount;

   @org.kie.api.definition.type.Label("Check for if we need to execute the rule against the amount")
   private java.lang.Boolean isDebitOrderInvestmentApplicable;

   public DebitOrderMinimum()
   {
   }

   public java.lang.Boolean getFail()
   {
      return this.fail;
   }

   public void setFail(java.lang.Boolean fail)
   {
      this.fail = fail;
   }

   public java.lang.String getMessage()
   {
      return this.message;
   }

   public void setMessage(java.lang.String message)
   {
      this.message = message;
   }

   public java.lang.String getElement()
   {
      return this.element;
   }

   public void setElement(java.lang.String element)
   {
      this.element = element;
   }

   public java.lang.Integer getCode()
   {
      return this.code;
   }

   public void setCode(java.lang.Integer code)
   {
      this.code = code;
   }

   public java.lang.String getSeverity()
   {
      return this.severity;
   }

   public void setSeverity(java.lang.String severity)
   {
      this.severity = severity;
   }

   public java.math.BigDecimal getInvestmentAmount()
   {
      return this.investmentAmount;
   }

   public void setInvestmentAmount(java.math.BigDecimal investmentAmount)
   {
      this.investmentAmount = investmentAmount;
   }

   public java.lang.Boolean getIsDebitOrderInvestmentApplicable()
   {
      return this.isDebitOrderInvestmentApplicable;
   }

   public void setIsDebitOrderInvestmentApplicable(
         java.lang.Boolean isDebitOrderInvestmentApplicable)
   {
      this.isDebitOrderInvestmentApplicable = isDebitOrderInvestmentApplicable;
   }

   public DebitOrderMinimum(java.lang.String message, java.lang.Boolean fail,
         java.lang.String element, java.lang.Integer code,
         java.lang.String severity, java.math.BigDecimal investmentAmount,
         java.lang.Boolean isDebitOrderInvestmentApplicable)
   {
      this.message = message;
      this.fail = fail;
      this.element = element;
      this.code = code;
      this.severity = severity;
      this.investmentAmount = investmentAmount;
      this.isDebitOrderInvestmentApplicable = isDebitOrderInvestmentApplicable;
   }
}